===============================
Tômbola: uma máquina de sortear
===============================

Uma tômbola nova vem descarregada::

    >>> from tombola import Tombola
    >>> t = Tombola()
    >>> t.carregada()
    False

A representação da Tômbola::

    >>> t  # doctest: +ELLIPSIS 
    <tombola.Tombola object at 0x...>


O método `carregar` fornece itens para sortear::

    >>> bolas = [1, 2, 3]
    >>> t.carregar(bolas)
    >>> t.carregada()
    True

Para testar o método `misturar`, usamos uma função mock, como uma dependência injetada no método::

::
    >>> def troca(lista):
    ...     lista[-2], lista[-1] = lista[-1], lista[-2]

    >>> t.misturar(troca)
    >>> t.itens
    [1, 3, 2]

Para sortear::

    >>> t.sortear()
    2
    >>> t.sortear()
    3
    >>> t.sortear()
    1
    >>> t.sortear()
    Traceback (most recent call last):
        ...
    LookupError: tombola vazia

A lista original de itens usada para carregar a tômbola não é alterada::

    >>> bolas
    [1, 2, 3]





