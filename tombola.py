# coding: utf-8

class Tombola(object):
	'''objeto sorteador sem repetições'''

	def __init__(self):
		self.itens = []

	def carregar(self, itens):
		self.itens = list(itens)

	def carregada(self):
		return bool(self.itens)

	def misturar(self, misturador):
		misturador(self.itens)

	def sortear(self):
		try:
			return self.itens.pop()
		except IndexError:
			raise LookupError('tombola vazia')
